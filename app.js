var express=require('express');
var bodyParser=require('body-parser');
var cookieParser= require('cookie-parser');
var passport=require('passport');
var session=require('express-session');

var port=process.env.PORT || 5000;
var app=express();

app.use(express.static('public'));
app.use(bodyParser.json({ extended: true }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(session({secret:'wedontneednoeducation'}));

require('./src/config/security/passport')(app);
app.set('views',('./src/views'));
app.set('view engine','ejs');

var userRouter=require('./src/routes/userRoutes');
app.use('/user',userRouter);
app.get('/',function(req,res){
  res.render('login/login');
});
app.listen(port,function(err){
  console.log('Working on '+port);
  if(err){
    console.log(err);
  }
});
