var express=require('express');
var userRouter=express.Router();
var passport=require('passport');
var userController=require('../controllers/user.controller');


userRouter.post('/signUp',function(req,res){
console.log(req.body);
userController.createUser(req,res);
});

userRouter.post('/signIn',passport.authenticate('local',{
  failureRedirect:'/'}),
  function(req,res){
    res.redirect('/user/profile');
  }
);

userRouter.get('/profile',function(req,res){
res.json(req.user);
});
module.exports=userRouter;
